#pragma once

#include <vector>
#include <string>
#include <variant>
#include <optional>
#include <algorithm>

using CellContent = std::variant<std::string, bool, float, int>;

///
/// \brief The Cell struct represents a cell in the table.
/// It has a key and a value attribute
///
struct Cell {
    CellContent key;
    CellContent value;
};
using Row = std::vector<Cell>;
using Sheet = std::vector<Row>;

///
/// \brief The DIFDocument class provides an API to parse DIF files.
///
/// Implementation detail:
/// All non-const member functions can move the iterator over the input.
class DIFDocument {
public:
    /**
     * @brief Parse a DIF-Document from the given string
     * @param string of DIF data
     * @param Whether the first row contains keys (headers of the columns)
     * @param Replacement list of keys if the document doesn't contain any
     */
    explicit DIFDocument(
            const std::string &dif,
            bool first_row_keys = false,
            std::optional<std::vector<CellContent>> row_keys = std::nullopt);

    /**
     * @brief Construct a DIF document from a Sheet (vector of vectors of Cells)
     * @param sheet
     */
    explicit DIFDocument(const Sheet &sheet);

    /**
     * converts a sheet to csv
     */
    static std::string sheet_to_csv_string(const Sheet &sheet);

    /**
     * returns the document as comma seperated values, for debug purposes
     */
    inline std::string to_csv_string() const {
        return DIFDocument::sheet_to_csv_string(m_sheet);
    }

    /**
     * Returns the column at the index
     */
    std::vector<CellContent> get_column(const int index) const;

    /**
     * Return the column with the specified key (header)
     */
    std::vector<CellContent> get_column(const CellContent &key) const;

    /**
     * Returns the column with the specified key (header), as a vector of a specifed type.
     * Of course this can only work if the column only contains cells of that type.
     */
    template<class T>
    std::vector<T> get_column(const CellContent &key) const {
        const auto in_vector = get_column(key);
        std::vector<T> out_vector = {};

        std::transform(in_vector.cbegin(), in_vector.cend(), std::back_inserter(out_vector), [](const CellContent &content) -> T {
            return std::get<T>(content);
        });

        return out_vector;
    };

    /**
     * Returns the content of the row at the specific index
     */
    std::vector<CellContent> get_row(const int index) const;

    /**
     * Returns the content of the row at the specific index as a vector of a specific type.
     * Of course this can only work if the row only contains cells of that type.
     */
    template<class T>
    std::vector<T> get_row(const int index) const {
        const auto in_vector = get_row(index);
        std::vector<T> out_vector = {};
        std::transform(in_vector.cbegin(), in_vector.cend(), std::back_inserter(out_vector), [](const CellContent &content) -> T {
            return std::get<T>(content);
        });

        return out_vector;
    }

    /**
     * Converts a cell_content to a string, even if is some kind of number
     */
    static std::string cell_content_to_string(const CellContent &value);

    /**
     * Returns the row of keys as an array of a specified type.
     * Of course this can only work if the row only contains cells of that type.
     */
    template<class T>
    std::vector<T> get_keys() const {
        const auto in_vector = get_keys();
        std::vector<T> out_vector = {};

        std::transform(in_vector.begin(), in_vector.cend(), std::back_inserter(out_vector), [](const CellContent &content) -> T {
            return std::get<T>(content);
        });

        return out_vector;
    }

    /**
     * @return number of columns specified in the file
     */
    inline int get_vectors() const
    {
        return m_vectors;
    }

    /**
     * @return number of rows specified in the file
     */
    inline bool get_tuples() const
    {
        return m_tuples;
    }

    /**
     * @return the row of keys
     */
    inline std::vector<CellContent> get_keys() const
    {
        return m_row_keys;
    }

    /**
     * @return the comment
     */
    inline std::string get_comment() const
    {
        return m_comment;
    }

    /**
     * @returns the sheet contents
     *
     * The format of the data is a vector of rows.
     * A row is a vector of Cell objects
     */
    inline Sheet sheet() const {
        return m_sheet;
    }

private:
    /**
     * Extract comment (which is most likely the sheet name if this .dif
     * was produced by a spreadsheet), vector and tuple counts.
     * DATA portion is ignored per spec.
     * Raise an error if any of the fields are missing or mal-formed.
     */
    void read_header_chunk();

    /**
     * Extract names of keys
     */
    void read_keys();

    /**
     * advances the internal iterator and returns the new content
     */
    std::string next() {
        ++m_iterator;
        return *m_iterator;
    }

    /**
     * establish row directive type and value
     */
    std::optional<std::vector<CellContent>> read_row();

    /**
     * Parse out value of the cell according to spec. Basically only
     * handles strings, integers and tuples.
     */
    CellContent cell_value(const std::string &type_and_value, std::string cmt) const;

    /**
     * parse columns from  m_tokens
     * return a list of Cell objects as row
     */
    std::optional<std::vector<Cell>> pack_row();

    /**
     * Removes quotes from the beginning and end of a string
     */
    static void remove_quotes(std::string &string);

    // input string split by newlines
    std::vector<std ::string> m_tokens = {};
    // Current position in the tokens list
    std::vector<std::string>::iterator m_iterator;

    // Row keys, either input from the user or autodetected
    std::vector<CellContent> m_row_keys = {};

    std::string m_comment;
    int m_vectors = 0;
    bool m_tuples = false;
    Sheet m_sheet = {};

    // Whether to use the first row as keys
    bool m_first_row_keys = false;
};
