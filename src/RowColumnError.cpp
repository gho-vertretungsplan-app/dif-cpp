#include "RowColumnError.h"

RowColumnError::RowColumnError(Error errror) noexcept
    : std::exception()
    , m_error(errror)
{

}

const char *RowColumnError::what() const noexcept
{
    switch (m_error) {
    case NameNotFound:
        return "Could not find row / column with the given name";
    }
}

RowColumnError::Error RowColumnError::error() const
{
    return m_error;
}
