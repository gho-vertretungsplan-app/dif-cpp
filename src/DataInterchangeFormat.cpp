﻿#include "DataInterchangeFormat.h"

#include <map>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cassert>

#include "DIFParsingError.h"
#include "RowColumnError.h"

DIFDocument::DIFDocument(const std::string &dif, bool first_row_keys, std::optional<std::vector<CellContent>> row_keys)
    : m_first_row_keys(first_row_keys)
{
    // If custom keys are passed, don't use first row
    if (row_keys) {
        m_first_row_keys = false;
        m_row_keys = *row_keys;
    }

    // Split into list of tokens
    std::istringstream f(dif);
    for (std::string line; std::getline(f, line, '\n');)
    {
        // file may end with an empty line
        if (!line.empty()) {
            m_tokens.push_back(line);
        }
    }

    // create iterator over tokens
    // start at -1 so we can use next() to get the first token
    m_iterator = m_tokens.begin() - 1;

    // Read header
    read_header_chunk();

    // Read keys
    if (!row_keys) {
        read_keys();
    }

    // Read data
    while (m_iterator != m_tokens.end()) {
        const auto row = pack_row();

        // some functions return nullopt
        if (row) {
            m_sheet.push_back(*row);
        }
    }
}

DIFDocument::DIFDocument(const Sheet &sheet)
    : m_sheet(sheet)
{
    // extract keys from first row
    std::transform(sheet.front().begin(), sheet.front().end(), std::back_inserter(m_row_keys), [](const Cell &cell) -> CellContent {
        return cell.key;
    });

    m_tuples = m_sheet.size();
    m_vectors = m_sheet.front().size();
}

std::string DIFDocument::sheet_to_csv_string(const Sheet &sheet)
{
    std::stringstream stream;

    // write header
    for (const Cell &item : sheet.front()) {
        stream << cell_content_to_string(item.key);
        stream << ";";
    }
    stream << std::endl;

    // write data
    for (const Row &row : sheet) {
        for (const Cell &cell : row) {
            stream << cell_content_to_string(cell.value);
            stream << ";";
        }
        stream << std::endl;
    };

    return stream.str();
}

void DIFDocument::read_header_chunk()
{
    struct Data {
        int value;
        std::string comment;
    };

    std::map<std::string, Data> d;

    for (int i = 0; i < 4; i++) {
        const std::string key = next();

        const std::string string = next();
        const std::string::size_type comma_pos = string.find_first_of(",");
        assert(comma_pos != std::string::npos);
        const int value = std::stoi(string.substr(comma_pos + 1));

        std::string comment = next();
        remove_quotes(comment);

        d[key] = Data {
            value,
            comment
        };
    };

    for (const auto i : {"TABLE", "VECTORS", "TUPLES"}) {
        if (d.find(i) == d.cend()) {
            throw DIFParsingError(DIFParsingError::InvalidHeaderChunk);
        }
    }

    m_comment = d["TABLE"].comment;
    m_vectors = d["VECTORS"].value;
    m_tuples = d["TUPLES"].value;
}

void DIFDocument::read_keys()
{
    if (!m_first_row_keys) {
        for (int i = 0; i < m_vectors; i++) {
            m_row_keys.push_back("col" + std::to_string(i));
        }
        return;
    }

    m_row_keys = *read_row();
}

std::optional<std::vector<CellContent>> DIFDocument::read_row()
{
    // Helpful for understanding the memory in the debugger
    //int index = std::distance(m_tokens.begin(), m_iterator);

    const std::string string = next();

    // document is over
    if (string.empty()) {
        return std::nullopt;
    }

    const std::string::size_type comma_pos = string.find_first_of(",");
    const int directive = std::stoi(string.substr(0, comma_pos));

    if (directive != -1) {
        throw DIFParsingError(DIFParsingError::MissingRowDirectiveType);
    }
    assert(directive == -1);

    const std::string bot_eod = next();
    if (bot_eod == "EOD") {
        return {};
    }

    // whoops. invalid format
    if (bot_eod != "BOT") {
        throw DIFParsingError(DIFParsingError::InvalidRowEnd);
    }
    assert(bot_eod == "BOT");

    std::vector<CellContent> contents;
    for (int i = 0; i < m_vectors; i++) {
        contents.push_back(cell_value(next(), next()));
    }

    assert(contents.size() != 0);
    return contents;
}

CellContent DIFDocument::cell_value(const std::string &type_and_value, std::string cmt) const
{
    const std::string::size_type comma_pos = type_and_value.find_first_of(",");
    assert(comma_pos != std::string::npos);
    const int type = std::stoi(type_and_value.substr(0, comma_pos + 1));

    const std::string value = type_and_value.substr(comma_pos + 1);

    remove_quotes(cmt);

    // we're numericish
    if (type == 0) {
        // are we valid
        if (cmt != "V") {
            return cmt;
        }

        // are we boolean
        if (value == "TRUE") {
            return true;
        }

        if (value == "FALSE") {
            return false;
        }

        // if . is part of value
        if (value.find('.') != std::string::npos) {
            return std::stof(value);
        }

        return std::stoi(value);
    }

    return cmt;
}

std::optional<std::vector<Cell>> DIFDocument::pack_row()
{
    std::vector<Cell> target;
    const auto row = read_row();

    if (!row) {
        return std::nullopt;
    }

    if (row->size() >= m_row_keys.size()) {
        for (size_t i = 0; i < m_row_keys.size(); i++) {
            const CellContent key = m_row_keys.at(i);
            const CellContent value = row->at(i);
            target.push_back(Cell {key, value});
        }

        return target;
    }

    return std::nullopt;
}

void DIFDocument::remove_quotes(std::string &string)
{
    // TODO C++20: Replace by starts_with()
    if (string.front() == '\"' && string.back() == '\"') {
        // everything but begin and end
        string = string.substr(1, string.size() - 2);
    }
    if (string.front() == '\'' && string.back() == '\'') {
        // everything but begin and end
        string = string.substr(1, string.size() - 2);
    }
}

std::vector<CellContent> DIFDocument::get_column(const int index) const
{
    std::vector<CellContent> column;
    for (const std::vector<Cell> &row : m_sheet) {
        if (row.size() == 0) {
            continue;
        }
        column.push_back(row.at(index).value);
    }

    return column;
}

std::vector<CellContent> DIFDocument::get_column(const CellContent &key) const
{
    const auto first_row = m_sheet.at(0);
    const auto it = std::find_if(first_row.begin(), first_row.end(), [&key](const Cell &cell) {
        return cell.key == key;
    });
    if (it == first_row.end()) {
        throw RowColumnError(RowColumnError::NameNotFound);
    }
    const int index_in_row = std::distance(first_row.begin(), it);

    return get_column(index_in_row);
}

std::vector<CellContent> DIFDocument::get_row(const int index) const
{
    const auto row = m_sheet.at(index);
    std::vector<CellContent> out_vector = {};

    std::transform(row.begin(), row.end(), std::back_inserter(out_vector), [](const Cell &content) {
        return content.value;
    });

    return out_vector;
}

std::string DIFDocument::cell_content_to_string(const CellContent &value)
{
    if (const auto *string = std::get_if<std::string>(&value)) {
        return *string;
    } else if (const auto *number = std::get_if<int>(&value)) {
        return std::to_string(*number);
    } else if (const auto *number = std::get_if<float>(&value)) {
        return std::to_string(*number);
    } else if (const auto *number = std::get_if<bool>(&value)) {
        return std::to_string(*number);
    }

    assert(false); // Unreachable
    return {};
}
