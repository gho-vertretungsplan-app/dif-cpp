#pragma once

#include <exception>

class RowColumnError : std::exception
{
public:
    enum Error {
        NameNotFound
    };

    RowColumnError(Error errror) noexcept;

    const char *what() const noexcept override;

    Error error() const;

private:
    Error m_error;
};
