#pragma once

#include <exception>

class DIFParsingError : std::exception
{
public:
    enum Error {
        InvalidHeaderChunk,
        InvalidRowEnd,
        MissingRowDirectiveType
    };

    DIFParsingError(Error error) noexcept;

    const char *what() const noexcept override;

    Error error() const;

private:
    Error m_error;
};
