#include "DIFParsingError.h"

DIFParsingError::DIFParsingError(Error error) noexcept
    : std::exception()
    , m_error(error)
{

}

const char *DIFParsingError::what() const noexcept
{
    switch (m_error) {
    case InvalidHeaderChunk:
        return "invalid header chunk";
    case InvalidRowEnd:
        return "invalid row end";
    case MissingRowDirectiveType:
        return "row directive type missing";
    }
}

DIFParsingError::Error DIFParsingError::error() const
{
    return m_error;
}
