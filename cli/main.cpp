#include "DataInterchangeFormat.h"
#include "CLI11/include/CLI/Formatter.hpp"
#include "CLI11/include/CLI/Config.hpp"
#include "CLI11/include/CLI/App.hpp"

#include <string>
#include <fstream>
#include <iostream>

std::string read_to_string(const std::string &path) {
    std::ifstream input(path);

    const std::string content((std::istreambuf_iterator<char>(input)),
                         std::istreambuf_iterator<char>());
    return content;
}

void write_to_file(const std::string &path, const std::string &string) {
    std::ofstream output(path);
    output << string;
}

int main(int argc, char *argv[]) {
    CLI::App cli_parser;
    std::string inpath;
    std::string outpath;
    std::vector<std::string> requested_keys;

    cli_parser.add_option("file", inpath, "Path to DIF file to read")->required();
    cli_parser.add_option("outfile", outpath, "Path to write the csv output to")->required();
    cli_parser.add_option("--columns", requested_keys, "Columns to output to the csv. All will be used if not specified")->allow_extra_args();

    CLI11_PARSE(cli_parser, argc, argv)

    // Start of program
    DIFDocument reader(read_to_string(inpath), true);

    if (requested_keys.size() == 0) {
        write_to_file(outpath, reader.to_csv_string());
    } else {
        Sheet target_sheet;

        // Sheet is a row based structure,
        // Append one element of the column to each row,
        // append new row if necessary
        for (const auto &key : requested_keys) {
            const std::vector<CellContent> col = reader.get_column(key);

            for (size_t i = 0; i < col.size(); i++) {
                if (target_sheet.size() <= i) {
                    target_sheet.push_back({});
                }

                target_sheet[i].push_back(Cell{key, col[i]});
            }
        }
        write_to_file(outpath, DIFDocument::sheet_to_csv_string(target_sheet));
    }
}
